import { PrismaClient } from '@prisma/client';
import { StocklyLibraryInterface } from './interface';
const prisma = new PrismaClient();

export class StocklyLibrary implements StocklyLibraryInterface {
  constructor() {}
  async deleteUserPermanent(userId: number) {
    try {
      const result = await prisma.s2_users.findFirst({
        where: {
          id: userId,
        },
        select: {
          id: true,
          firstname: true,
        },
      });
      if (result) {
        return {
          status: 200,
          message: 'sucess',
          data: result,
        };
      }
      return {
        status: 400,
        message: 'sucess',
        data: result,
      };
    } catch (error) {
      return {
        status: 500,
        message: error,
        data: null,
      };
    }
  }
}
