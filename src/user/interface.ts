import { response } from '../common/response';

export interface StocklyLibraryInterface {
  deleteUserPermanent(userId: number): Promise<response>;
}
