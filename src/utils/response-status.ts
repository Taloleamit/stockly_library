export class ResponseStatus {
  success = 'Success';
  fail = 'Fail';
  error = 'Error';
  unauthorized = 'Unauthorized';
  notFound = 'Not found';
  badRequest = 'Bad request';
  httpTooManyRequests = 'Too many request';
  emailAddressNotExists = 'Email address not exists';
  dataNotFound = 'Data not found';
  emailAddressAlreadyExists = 'Email address already exists';
  emailSendFail = 'Email Send Fail';
  emailVerificationSuccess = 'Email verification success';
  userAlreadyExists = 'User already exists';
  categoryAlreadyExists = 'Category already exists';
  mailSentSuccessfully = 'Mail sent successfully';
  invalidParams = 'invalid parameter';
  pollOptionRemoved = 'poll option removed';
  pollOptionChanged = 'poll option changed';
  pollOptionAdded = 'poll option added';
  invaliadData = 'Quantity and Avarage Price Should be Greater than  Zero';
  quantityStockError = 'Selling stocks are more than the purchased stocks';
  notSupportedForCountry = 'Not supported for this Country';
  quantityError = 'This Stocks is Not purchased by you';
  teamBannerIdRequried = 'Team bannerId requried';
  usersAlreadyBlocked = 'User already blocked';
  AlreadyReported = 'Already Reported On This User';
}

export class StatusCode {
  success = 200;
  error = 500;
  notFound = 404;
  unauthorized = 401;
  conflict = 409;
  created = 201;
  bad = 400;
  noContent = 204;
}
