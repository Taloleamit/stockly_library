import { response } from './response';

export interface CommonInterface {
  getFullName(firstName: string, lastName: string): Promise<response>;
}
