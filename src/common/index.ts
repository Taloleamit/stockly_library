import { ResponseStatus, StatusCode } from '../utils/response-status';
import { CommonInterface } from './types';

const statusCode = new StatusCode();
const responseStatus = new ResponseStatus();

export class Common implements CommonInterface {
  constructor() {}
  async getFullName(firstName: string, lastName: string) {
    return {
      status: statusCode.success,
      message: responseStatus.success,
      data: `${firstName || ''}` + ' ' + `${lastName || ''}`,
    };
  }
}
